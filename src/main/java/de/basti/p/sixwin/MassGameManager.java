package de.basti.p.sixwin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Preconditions;

public class MassGameManager {

	private final List<? extends IPlayerStrategy> players;
	private final int numberOfGames;
	private final Map<IPlayerStrategy, Double> strategyToPointsMap;
	private final Map<IPlayerStrategy, Integer> strategyToWinsMap;

	public MassGameManager(List<? extends IPlayerStrategy> players, int numberOfGames) {
		super();
		Preconditions.checkArgument(players.size() == 4, "We need exact 4 players");
		this.players = players;
		this.numberOfGames = numberOfGames;
		this.strategyToPointsMap = new HashMap<>();
		this.strategyToWinsMap = new HashMap<>();
		players.forEach(p -> strategyToPointsMap.put(p, 0d));
		players.forEach(p -> strategyToWinsMap.put(p, 0));
	}

	public GameResult runGames() {
		GameResult result = new GameResult(strategyToPointsMap, strategyToWinsMap, 0);
		int stepSize = numberOfGames / 10;
		for (int i = 0; i < numberOfGames; i++) {
			if (i % stepSize == 0) {
				System.out.println(i + " Games simulated.");
			}
			players.stream().forEach(p -> p.reset());
			GameManager gm = new GameManager(players, false);
			result = result.merge(gm.runGame());
		}
		return result;
	}

}
