package de.basti.p.sixwin;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import de.basti.p.sixwin.viewer.BoardViewer;
import de.basti.p.sixwin.viewer.SlotViewer;

public interface IPlayerStrategy {

	public void reset();

	public String getName();

	public Card chooseCard(BoardViewer boardViewer, Set<Card> possibleCards);

	public void cardsPlayed(List<Card> cards, Integer points);

	public SlotViewer chooseSlot(BoardViewer boardViewer, Collection<Card> choosedCardsOfAllPlayers,
			Set<Card> remaingCards);
}
