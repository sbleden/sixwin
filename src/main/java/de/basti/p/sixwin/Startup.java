package de.basti.p.sixwin;

import java.util.List;

import com.google.common.collect.Lists;

import de.basti.p.sixwin.strategy.RandomStrategy;
import de.basti.p.sixwin.strategy.SimpleStrategy;
import de.basti.p.sixwin.strategy.ki.KIStrategyFactory;

public class Startup {

	public static void main(String[] args) {

		List<IPlayerStrategy> strategies = Lists.newArrayList(new RandomStrategy(), KIStrategyFactory.loadV1Ki(),
				new SimpleStrategy(), new RandomStrategy());
		MassGameManager gameManager = new MassGameManager(strategies, 5_000);
		GameResult result = gameManager.runGames();
		System.out.println(result);
	}

}
