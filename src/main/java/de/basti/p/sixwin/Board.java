package de.basti.p.sixwin;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.UUID;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class Board {

	private final List<Slot> slots;
	private final LinkedHashSet<Card> cards;

	protected Board(int numberOfSlots, int numberOfCards) {
		CardFactory cardFactory = new CardFactory();
		this.cards = Sets.newLinkedHashSet();
		for (int i = 1; i <= numberOfCards; i++) {
			cards.add(cardFactory.generateCard(i));
		}
		this.slots = Lists.newArrayList();
		for (int i = 0; i < numberOfSlots; i++) {
			slots.add(new Slot(getAndRemoveRandomCardFromDeck()));
		}
	}

	public List<Slot> getSlots() {
		return slots;
	}

	public Slot getSlotWithID(UUID id) {
		return slots.stream().filter(slot -> Objects.equal(slot.getId(), id)).findFirst().get();
	}

	public void replaceSlot(Slot slot, Card card) {
		int index = slots.indexOf(slot);
		slots.set(index, new Slot(card));
	}

	public Card getAndRemoveRandomCardFromDeck() {
		int index = (int) (Math.random() * cards.size());
		Card card = Lists.newArrayList(cards).get(index);
		cards.remove(card);
		return card;
	}

	@Override
	public String toString() {
		String s = "";
		for (Slot slot : slots) {
			s += slot.toString() + "\n";
		}
		return s;
	}

}
