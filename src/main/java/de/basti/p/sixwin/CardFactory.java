package de.basti.p.sixwin;

import java.util.List;
import java.util.function.Function;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

public class CardFactory {

	private List<Function<Integer, Integer>> hornFunctions = Lists.newArrayList();

	protected CardFactory() {
		hornFunctions.add(x -> x % 5 == 0 ? 2 : 0);
		hornFunctions.add(x -> x % 10 == 0 ? 3 : 0);
		hornFunctions.add(x -> x % 11 == 0 ? 5 : 0);
	}

	protected Card generateCard(int number) {
		Preconditions.checkArgument(number > 0);
		Integer horns = hornFunctions.stream().map(f -> f.apply(number)).reduce(0, (x, y) -> x + y);
		if (horns == 0) {
			horns = 1;
		}
		return new Card(number, horns);
	}

}
