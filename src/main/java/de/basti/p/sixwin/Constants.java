package de.basti.p.sixwin;

public class Constants {

	public static final int MAX_SLOT_SIZE = 6;
	public static final int NUMBER_OF_SLOTS = 5;
	public static final int NUMBER_OF_PLAYERS = 4;
	public static final int NUMBER_OF_CARDS = 100;
	public static final int NUMBER_OF_CARDS_PER_PLAYER = 10;
}
