package de.basti.p.sixwin.strategy.ki.model;

import java.util.List;
import java.util.Set;

import de.basti.p.sixwin.Card;
import de.basti.p.sixwin.viewer.SlotViewer;

public class State {

	private List<SlotViewer> slots;
	private Set<Card> possibleCards;

	public State(List<SlotViewer> slots, Set<Card> possibleCards) {
		super();
		this.slots = slots;
		this.possibleCards = possibleCards;
	}

	public List<SlotViewer> getSlots() {
		return slots;
	}

	public Set<Card> getPossibleCards() {
		return possibleCards;
	}
}
