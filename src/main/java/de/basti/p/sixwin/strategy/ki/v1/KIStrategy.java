package de.basti.p.sixwin.strategy.ki.v1;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import de.basti.p.sixwin.Card;
import de.basti.p.sixwin.IPlayerStrategy;
import de.basti.p.sixwin.strategy.ki.AFeatureBasedStrategy;
import de.basti.p.sixwin.strategy.ki.features.IFeatureExtractor;
import de.basti.p.sixwin.strategy.ki.model.CardFeatureValues;
import de.basti.p.sixwin.viewer.BoardViewer;
import de.basti.p.sixwin.viewer.SlotViewer;

public class KIStrategy extends AFeatureBasedStrategy implements IPlayerStrategy {

	private final CardChoosingNetwork network;
	private String name;

	public KIStrategy(String name, CardChoosingNetwork network, List<IFeatureExtractor> featureExtractors) {
		super(featureExtractors);
		this.name = name;
		this.network = network;
	}

	@Override
	public void reset() {
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Card chooseCard(BoardViewer boardViewer, Set<Card> possibleCards) {
		List<CardFeatureValues> cardFeatureValues = extractCardFeatureValues(boardViewer, possibleCards);

		List<Double> evaluate = network.evaluate(cardFeatureValues).subList(0, cardFeatureValues.size());
		double max = evaluate.stream().reduce(0d, Math::max);
		for (int i = 0; i < evaluate.size(); i++) {
			Double d = evaluate.get(i);
			if (Math.abs(max - d) < 0.001) {
				return cardFeatureValues.get(i).getC();
			}
		}
		throw new IllegalArgumentException("Could not determine card.");
	}

	@Override
	public void cardsPlayed(List<Card> cards, Integer points) {
	}

	@Override
	public SlotViewer chooseSlot(BoardViewer boardViewer, Collection<Card> choosedCardsOfAllPlayers,
			Set<Card> remaingCards) {
		return boardViewer.getSlots().stream().sorted((s1, s2) -> s1.getNumberOfAllHorns() - s2.getNumberOfAllHorns())
				.findFirst().get();
	}
}