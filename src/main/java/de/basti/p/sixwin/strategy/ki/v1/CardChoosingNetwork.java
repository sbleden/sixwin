package de.basti.p.sixwin.strategy.ki.v1;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;

import com.google.common.base.Preconditions;

import de.basti.p.sixwin.strategy.ki.model.CardFeatureValues;
import de.basti.p.sixwin.strategy.ki.model.SituationFeatureValues;

public class CardChoosingNetwork {

	private MultiLayerNetwork net;
	private int numInputs;
	private int numOutputs;

	public CardChoosingNetwork(int numInputs, int numOutputs, Supplier<MultiLayerNetwork> netSupplier) {
		this.numInputs = numInputs;
		this.numOutputs = numOutputs;
		this.net = netSupplier.get();
	}

	public void fit(List<SituationFeatureValues> situations) {

		INDArray featureArray = toFeatureArray(situations);
		INDArray labelArray = toLabelArray(situations);

		DataSet dataSet = new DataSet(featureArray, labelArray);

		net.fit(dataSet);
	}

	public MultiLayerNetwork getNet() {
		return net;
	}

	public List<Double> evaluate(List<CardFeatureValues> cardFeatureValues) {

		INDArray input = Nd4j.zeros(1, numInputs);
		fillCardFeatureValues(input, 0, cardFeatureValues);
		INDArray result = net.output(input, false);
		Preconditions.checkState(result.shape()[1] >= cardFeatureValues.size());

		List<Double> resultList = IntStream.range(0, numOutputs).mapToObj(i -> result.getDouble(i)).collect(toList());
		Preconditions.checkState(resultList.size() >= cardFeatureValues.size());
		return resultList;
	}

	private INDArray toFeatureArray(List<SituationFeatureValues> situations) {
		Preconditions.checkState(situations.get(0).getDataSize() == numInputs);
		INDArray input = Nd4j.zeros(situations.size(), numInputs);
		for (int i = 0; i < situations.size(); i++) {
			SituationFeatureValues featureValues = situations.get(i);
			List<CardFeatureValues> cardValues = featureValues.getCardValues();
			fillCardFeatureValues(input, i, cardValues);
		}
		return input;
	}

	private void fillCardFeatureValues(INDArray input, int i, List<CardFeatureValues> cardValues) {
		for (int k = 0; k < cardValues.size(); k++) {
			CardFeatureValues cardFeatureValues = cardValues.get(k);
			for (int f = 0; f < cardFeatureValues.getFeaturesValues().size(); f++) {
				input.putScalar(new int[] { i, k * cardFeatureValues.getFeaturesValues().size() + f },
						cardFeatureValues.getFeaturesValues().get(f));
			}
		}
	}

	private INDArray toLabelArray(List<SituationFeatureValues> situations) {

		Preconditions.checkState(situations.get(0).getLabelSize() == numOutputs);
		INDArray labels = Nd4j.zeros(situations.size(), numOutputs);

		for (int i = 0; i < situations.size(); i++) {
			SituationFeatureValues featureValues = situations.get(i);
			for (int l = 0; l < featureValues.getCardValues().size(); l++) {
				boolean result = featureValues.getCardValues().get(l).getC().getNumber() == featureValues
						.getChoosedCard().getNumber();
				labels.putScalar(new int[] { i, l }, result ? 1 : 0);
			}
		}
		return labels;
	}

}
