package de.basti.p.sixwin.strategy.ki.features;

import de.basti.p.sixwin.Constants;
import de.basti.p.sixwin.strategy.ki.model.Action;
import de.basti.p.sixwin.strategy.ki.model.State;

public class CardValueExtractor implements IFeatureExtractor {

	@Override
	public double extractFeatureValue(State s, Action a) {
		return (Constants.NUMBER_OF_CARDS - a.getCard().getNumber()) * 1.0 / Constants.NUMBER_OF_CARDS;
	}

}
