package de.basti.p.sixwin.strategy.ki;

import static java.util.stream.Collectors.toList;

import java.util.Comparator;
import java.util.List;
import java.util.Set;

import de.basti.p.sixwin.Card;
import de.basti.p.sixwin.strategy.ki.features.IFeatureExtractor;
import de.basti.p.sixwin.strategy.ki.model.Action;
import de.basti.p.sixwin.strategy.ki.model.CardFeatureValues;
import de.basti.p.sixwin.strategy.ki.model.State;
import de.basti.p.sixwin.viewer.BoardViewer;

public class AFeatureBasedStrategy {

	protected final List<IFeatureExtractor> featureExtractors;

	public AFeatureBasedStrategy(List<IFeatureExtractor> featureExtractors) {
		super();
		this.featureExtractors = featureExtractors;
	}

	protected List<CardFeatureValues> extractCardFeatureValues(BoardViewer boardViewer, Set<Card> possibleCards) {
		State s = new State(boardViewer.getSlots(), possibleCards);
		List<CardFeatureValues> cardFeatureValues = possibleCards.stream().sorted(Comparator.comparing(Card::getNumber))
				.map(card -> {

					List<Double> featureValues = featureExtractors.stream()
							.map(f -> f.extractFeatureValue(s, new Action(card))).collect(toList());
					return new CardFeatureValues(featureValues, card);
				}).collect(toList());
		return cardFeatureValues;
	}

}