package de.basti.p.sixwin.strategy.ki.model;

import java.util.List;

import de.basti.p.sixwin.Card;

public class SituationFeatureValues {

	private List<CardFeatureValues> cardValues;
	private Card choosedCard;
	private Integer points;

	public SituationFeatureValues(List<CardFeatureValues> cardValues, Card choosedCard) {
		super();
		this.cardValues = cardValues;
		this.choosedCard = choosedCard;
	}

	public List<CardFeatureValues> getCardValues() {
		return cardValues;
	}

	public Card getChoosedCard() {
		return choosedCard;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public int getDataSize() {
		return cardValues.size() * cardValues.get(0).getFeaturesValues().size();
	}

	public int getLabelSize() {
		return cardValues.size();
	}

}
