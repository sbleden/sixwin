package de.basti.p.sixwin.strategy.ki.features.plain;

import de.basti.p.sixwin.strategy.ki.features.IFeatureExtractor;

public abstract class ACardFeatureExtractor implements IFeatureExtractor {

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

}
