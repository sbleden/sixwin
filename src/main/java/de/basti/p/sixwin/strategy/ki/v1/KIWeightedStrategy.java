package de.basti.p.sixwin.strategy.ki.v1;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import de.basti.p.sixwin.Card;
import de.basti.p.sixwin.IPlayerStrategy;
import de.basti.p.sixwin.strategy.ki.AFeatureBasedStrategy;
import de.basti.p.sixwin.strategy.ki.features.IFeatureExtractor;
import de.basti.p.sixwin.strategy.ki.model.CardFeatureValues;
import de.basti.p.sixwin.viewer.BoardViewer;
import de.basti.p.sixwin.viewer.SlotViewer;

public class KIWeightedStrategy extends AFeatureBasedStrategy implements IPlayerStrategy {

	private final CardChoosingNetwork network;

	public KIWeightedStrategy(CardChoosingNetwork network, List<IFeatureExtractor> featureExtractors) {
		super(featureExtractors);
		this.network = network;
	}

	@Override
	public void reset() {
	}

	@Override
	public String getName() {
		return KIWeightedStrategy.class.getName();
	}

	@Override
	public Card chooseCard(BoardViewer boardViewer, Set<Card> possibleCards) {
		List<CardFeatureValues> cardFeatureValues = extractCardFeatureValues(boardViewer, possibleCards);

		List<Double> evaluate = network.evaluate(cardFeatureValues).subList(0, cardFeatureValues.size());
		double sum = evaluate.stream().reduce(0d, (d1, d2) -> d1 + d2);
		double random = Math.random() * sum;
		double currentValue = 0;
		for (int i = 0; i < evaluate.size(); i++) {
			Double d = evaluate.get(i);
			currentValue += d;
			if (currentValue > random) {
				return cardFeatureValues.get(i).getC();
			}
		}
		throw new IllegalArgumentException("Could not determine card.");
	}

	@Override
	public void cardsPlayed(List<Card> cards, Integer points) {
	}

	@Override
	public SlotViewer chooseSlot(BoardViewer boardViewer, Collection<Card> choosedCardsOfAllPlayers,
			Set<Card> remaingCards) {
		return boardViewer.getSlots().stream().sorted((s1, s2) -> s1.getNumberOfAllHorns() - s2.getNumberOfAllHorns())
				.findFirst().get();
	}

}
