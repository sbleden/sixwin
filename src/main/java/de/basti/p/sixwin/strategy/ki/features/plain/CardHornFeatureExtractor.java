package de.basti.p.sixwin.strategy.ki.features.plain;

import de.basti.p.sixwin.strategy.ki.model.Action;
import de.basti.p.sixwin.strategy.ki.model.State;

public class CardHornFeatureExtractor extends ACardFeatureExtractor {

	@Override
	public double extractFeatureValue(State s, Action a) {
		return a.getCard().getHorns();
	}

}
