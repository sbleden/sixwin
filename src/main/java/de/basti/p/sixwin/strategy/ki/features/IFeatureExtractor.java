package de.basti.p.sixwin.strategy.ki.features;

import de.basti.p.sixwin.strategy.ki.model.Action;
import de.basti.p.sixwin.strategy.ki.model.State;

public interface IFeatureExtractor {

	public double extractFeatureValue(State s, Action a);

	public default IFeatureExtractor square() {
		return (s, a) -> Math.pow(extractFeatureValue(s, a), 2);
	}
}
