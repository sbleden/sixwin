package de.basti.p.sixwin.strategy.ki.deprecated;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.api.IterationListener;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.learning.config.RmsProp;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import com.google.gson.Gson;

import de.basti.p.sixwin.strategy.ki.model.CardFeatureValues;
import de.basti.p.sixwin.strategy.ki.model.SituationFeatureValues;

public class LearningPlayground {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		String json = IOUtils.toString(new FileInputStream(new File("data.json")));
		SituationFeatureValues[] data = new Gson().fromJson(json, SituationFeatureValues[].class);
		doLearn(data);
	}

	public static void doLearn(SituationFeatureValues[] data) throws IOException {
		int dataNumber = data.length;
		System.out.println(dataNumber);
		int numFeatureSize = data[0].getDataSize();
		int numLabelSize = data[0].getLabelSize();

		INDArray input = getInput(data, dataNumber, numFeatureSize);
		INDArray labels = ceateLabels(data, dataNumber, numLabelSize);

		DataSet ds = new DataSet(input, labels);

		int seed = 1235;
		MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
				.optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT).seed(seed).updater(new Nesterovs())
				.list()//
				.layer(0, createDenseLayer(numFeatureSize, numFeatureSize))//
				.layer(1, createDenseLayer(numFeatureSize, numFeatureSize))//
				.layer(2, createDenseLayer(numFeatureSize, numFeatureSize))//
				.layer(3, createOutputLayer(numFeatureSize, numLabelSize))//
				.pretrain(false).backprop(true).build();

		MultiLayerNetwork network = new MultiLayerNetwork(conf);
		network.init();
		network.setListeners(new ScoreIterationListener(1));

		// Initialize the user interface backend
		UIServer uiServer = UIServer.getInstance();

		// Configure where the network information (gradients, score vs. time etc) is to
		// be stored. Here: store in memory.
		StatsStorage statsStorage = new InMemoryStatsStorage(); // Alternative: new FileStatsStorage(File), for saving
																// and loading later

		// Attach the StatsStorage instance to the UI: this allows the contents of the
		// StatsStorage to be visualized
		uiServer.attach(statsStorage);

		// Then add the StatsListener to collect this information from the network, as
		// it trains
		network.setListeners(new StatsListener(statsStorage));
		ds.shuffle();
		org.nd4j.linalg.dataset.api.DataSet train = ds.getRange(0, (int) (dataNumber * 0.3));
		org.nd4j.linalg.dataset.api.DataSet validation = ds.getRange((int) (dataNumber * 0.3), dataNumber);
		network.addListeners(new IterationListener() {

			@Override
			public boolean invoked() {
				return false;
			}

			@Override
			public void invoke() {
			}

			@Override
			public void iterationDone(Model model, int iteration) {
				if (iteration % 100 == 0) {
					Evaluation eval = createEvaluator(numLabelSize, network, validation);
					System.out.println(eval.stats());
				}
			}

		});
		for (int i = 0; i < 100000; i++) {
			network.fit(train);
		}
		// create output for every training sample
		INDArray output = network.output(validation.getFeatures());
		System.out.println(output);
		Evaluation eval = createEvaluator(numLabelSize, network, validation);
		System.out.println(eval.stats());
	}

	private static Evaluation createEvaluator(int numLabelSize, MultiLayerNetwork network,
			org.nd4j.linalg.dataset.api.DataSet validation) {
		INDArray output = network.output(validation.getFeatures());
		List<String> list = new ArrayList<>(numLabelSize);
		for (int i = 0; i < numLabelSize; i++) {
			list.add(String.valueOf(i));
		}
		Evaluation eval = new Evaluation(list, 3);
		eval.eval(validation.getLabels(), output);
		return eval;
	}

	private static INDArray ceateLabels(SituationFeatureValues[] data, int dataNumber, int numLabelSize) {
		INDArray labels = Nd4j.zeros(dataNumber, numLabelSize);

		for (int i = 0; i < dataNumber; i++) {
			SituationFeatureValues featureValues = data[i];
			for (int l = 0; l < featureValues.getCardValues().size(); l++) {
				boolean result = featureValues.getCardValues().get(l).getC().getNumber() == featureValues
						.getChoosedCard().getNumber();
				labels.putScalar(new int[] { i, l }, result ? 1 : 0);
			}
		}
		return labels;
	}

	private static INDArray getInput(SituationFeatureValues[] data, int dataNumber, int numFeatureSize) {
		INDArray input = Nd4j.zeros(dataNumber, numFeatureSize);
		for (int i = 0; i < dataNumber; i++) {
			SituationFeatureValues featureValues = data[i];
			for (int k = 0; k < featureValues.getCardValues().size(); k++) {
				CardFeatureValues cardFeatureValues = featureValues.getCardValues().get(k);
				for (int f = 0; f < cardFeatureValues.getFeaturesValues().size(); f++) {
					input.putScalar(new int[] { i, k * cardFeatureValues.getFeaturesValues().size() + f },
							cardFeatureValues.getFeaturesValues().get(f));
				}
			}
		}
		return input;
	}

	private static OutputLayer createOutputLayer(int numFeatureSize, int numLabelSize) {
		return new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD).nIn(numFeatureSize)
				.nOut(numLabelSize).activation(Activation.SOFTMAX).weightInit(WeightInit.XAVIER).build();
	}

	private static DenseLayer createDenseLayer(int in, int out) {
		return new DenseLayer.Builder().nIn(in) // Number
												// of
												// input
												// datapoints.
				.nOut(out) // Number of output datapoints.
				.activation(Activation.TANH) // Activation
												// function.
				.weightInit(WeightInit.XAVIER) // Weight
				// initialization.
				.build();
	}
}
