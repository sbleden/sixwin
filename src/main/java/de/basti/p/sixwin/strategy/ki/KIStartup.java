package de.basti.p.sixwin.strategy.ki;

import java.io.IOException;
import java.util.List;

import com.beust.jcommander.internal.Lists;

import de.basti.p.sixwin.Constants;
import de.basti.p.sixwin.strategy.ki.features.CardValueExtractor;
import de.basti.p.sixwin.strategy.ki.features.ChoosedSlotSize;
import de.basti.p.sixwin.strategy.ki.features.IFeatureExtractor;
import de.basti.p.sixwin.strategy.ki.features.MinDifExtractor;
import de.basti.p.sixwin.strategy.ki.v1.CardChoosingNetwork;

public class KIStartup {

	public static void main(String[] args) throws IOException {
		List<IFeatureExtractor> featureExtractors = Lists.newArrayList();
		featureExtractors.add(new CardValueExtractor());
		featureExtractors.add(new ChoosedSlotSize());
		featureExtractors.add(new MinDifExtractor());

		int numOutputs = Constants.NUMBER_OF_CARDS_PER_PLAYER;
		int numInputs = Constants.NUMBER_OF_CARDS_PER_PLAYER * featureExtractors.size();
		CardChoosingNetwork network = new CardChoosingNetwork(numInputs, numOutputs,
				() -> NetFactory.createNetwork(true, numInputs, numOutputs));

		KILearner learner = new KILearner(featureExtractors, network);
		learner.doRun();
	}
}
