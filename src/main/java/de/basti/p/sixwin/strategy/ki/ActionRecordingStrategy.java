package de.basti.p.sixwin.strategy.ki;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;

import de.basti.p.sixwin.Card;
import de.basti.p.sixwin.IPlayerStrategy;
import de.basti.p.sixwin.strategy.ki.features.IFeatureExtractor;
import de.basti.p.sixwin.strategy.ki.model.CardFeatureValues;
import de.basti.p.sixwin.strategy.ki.model.SituationFeatureValues;
import de.basti.p.sixwin.viewer.BoardViewer;
import de.basti.p.sixwin.viewer.SlotViewer;

public class ActionRecordingStrategy extends AFeatureBasedStrategy implements IPlayerStrategy {

	private final IPlayerStrategy baseStrategy;
	private final List<SituationFeatureValues> actions;

	public ActionRecordingStrategy(IPlayerStrategy baseStrategy, List<IFeatureExtractor> featureExtractors) {
		super(featureExtractors);
		this.baseStrategy = baseStrategy;
		this.actions = Lists.newArrayList();
	}

	@Override
	public void reset() {
		this.actions.clear();
	}

	@Override
	public String getName() {
		return ActionRecordingStrategy.class.getName() + ":" + baseStrategy.getName();
	}

	@Override
	public Card chooseCard(BoardViewer boardViewer, Set<Card> possibleCards) {
		List<CardFeatureValues> cardFeatureValues = extractCardFeatureValues(boardViewer, possibleCards);
		Card chooseCard = baseStrategy.chooseCard(boardViewer, possibleCards);
		actions.add(new SituationFeatureValues(cardFeatureValues, chooseCard));
		return chooseCard;
	}

	@Override
	public void cardsPlayed(List<Card> cards, Integer points) {
		actions.get(actions.size() - 1).setPoints(points);
	}

	@Override
	public SlotViewer chooseSlot(BoardViewer boardViewer, Collection<Card> choosedCardsOfAllPlayers,
			Set<Card> remaingCards) {
		return baseStrategy.chooseSlot(boardViewer, choosedCardsOfAllPlayers, remaingCards);
	}

	public List<SituationFeatureValues> getActions() {
		return actions;
	}

	public void clearActions() {
		actions.clear();
	}

}
