package de.basti.p.sixwin.strategy.ki.features.plain;

import de.basti.p.sixwin.strategy.ki.model.Action;
import de.basti.p.sixwin.strategy.ki.model.State;

public class SlotHighestNumber extends ASlotFeatureExtractor {

	public SlotHighestNumber(int slot) {
		super(slot);
	}

	@Override
	public double extractFeatureValue(State s, Action a) {

		return s.getSlots().get(slot).getHighestNumber();
	}
}
