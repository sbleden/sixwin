package de.basti.p.sixwin.strategy.ki.features;

import java.util.List;
import java.util.Optional;

import de.basti.p.sixwin.Card;
import de.basti.p.sixwin.Constants;
import de.basti.p.sixwin.strategy.ki.model.Action;
import de.basti.p.sixwin.strategy.ki.model.State;
import de.basti.p.sixwin.viewer.SlotViewer;

public class ChoosedSlotSize implements IFeatureExtractor {

	@Override
	public double extractFeatureValue(State s, Action a) {
		return getSlotToAppend(s.getSlots(), a.getCard())
				.map(slot -> (Constants.MAX_SLOT_SIZE - slot.getCards().size()) * 1.0 / Constants.MAX_SLOT_SIZE)
				.orElse(0.0);
	}

	private Optional<SlotViewer> getSlotToAppend(List<SlotViewer> slots, Card card) {
		return slots.stream().filter(slot -> slot.getHighestNumber() < card.getNumber())
				.sorted((s1, s2) -> -s1.getHighestNumber() + s2.getHighestNumber()).findFirst();
	}
}
