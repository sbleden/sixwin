package de.basti.p.sixwin.strategy.ki;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;

import de.basti.p.sixwin.GameManager;
import de.basti.p.sixwin.GameResult;
import de.basti.p.sixwin.IPlayerStrategy;
import de.basti.p.sixwin.strategy.RandomStrategy;
import de.basti.p.sixwin.strategy.SimpleStrategy;
import de.basti.p.sixwin.strategy.ki.features.IFeatureExtractor;
import de.basti.p.sixwin.strategy.ki.model.SituationFeatureValues;

public class KILearngDataExtractor {

	private int numDataSets;
	private boolean winnsOnly;
	private List<IFeatureExtractor> featureExtractors;
	private IPlayerStrategy baseStrategy;

	public KILearngDataExtractor(int numDataSets, boolean winnsOnly, List<IFeatureExtractor> featureExtractors) {
		super();
		this.numDataSets = numDataSets;
		this.winnsOnly = winnsOnly;
		this.featureExtractors = featureExtractors;
		this.baseStrategy = new RandomStrategy();
	}

	public void setBaseStrategy(IPlayerStrategy baseStrategy) {
		this.baseStrategy = baseStrategy;
	}

	public List<SituationFeatureValues> learn() {

		int steps = numDataSets / 10;

		List<SituationFeatureValues> bestActions = Lists.newArrayList();

		while (bestActions.size() < numDataSets) {

			if (bestActions.size() % steps == 0) {
				System.out.println("Sets: " + bestActions.size() + " of " + numDataSets);
			}

			ActionRecordingStrategy strategy = new ActionRecordingStrategy(baseStrategy, featureExtractors);

			ArrayList<IPlayerStrategy> players = Lists.newArrayList(strategy, baseStrategy, new SimpleStrategy(),
					new SimpleStrategy());
			GameManager gameManager = new GameManager(players, false);
			GameResult gameResult = gameManager.runGame();

			if (!winnsOnly || gameResult.getWinner() == strategy) {
				bestActions.addAll(strategy.getActions());
			}

		}
		return bestActions;
	}
}
