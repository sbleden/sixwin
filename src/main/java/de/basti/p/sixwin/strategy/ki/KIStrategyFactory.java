package de.basti.p.sixwin.strategy.ki;

import java.util.List;

import com.beust.jcommander.internal.Lists;

import de.basti.p.sixwin.Constants;
import de.basti.p.sixwin.IPlayerStrategy;
import de.basti.p.sixwin.strategy.ki.features.CardValueExtractor;
import de.basti.p.sixwin.strategy.ki.features.ChoosedSlotSize;
import de.basti.p.sixwin.strategy.ki.features.IFeatureExtractor;
import de.basti.p.sixwin.strategy.ki.features.MinDifExtractor;
import de.basti.p.sixwin.strategy.ki.v1.CardChoosingNetwork;
import de.basti.p.sixwin.strategy.ki.v1.KIStrategy;

public class KIStrategyFactory {

	public static IPlayerStrategy loadV1Ki() {

		List<IFeatureExtractor> featureExtractors = Lists.newArrayList();
		featureExtractors.add(new CardValueExtractor());
		featureExtractors.add(new ChoosedSlotSize());
		featureExtractors.add(new MinDifExtractor());

		int numOutputs = Constants.NUMBER_OF_CARDS_PER_PLAYER;
		int numInputs = Constants.NUMBER_OF_CARDS_PER_PLAYER * featureExtractors.size();

		CardChoosingNetwork network = new CardChoosingNetwork(numInputs, numOutputs,
				() -> NetFactory.loadNetwork("v1.model"));
		return new KIStrategy("KI V1", network, featureExtractors);
	}

}
