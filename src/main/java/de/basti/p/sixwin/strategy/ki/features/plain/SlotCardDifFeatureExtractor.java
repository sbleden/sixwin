package de.basti.p.sixwin.strategy.ki.features.plain;

import de.basti.p.sixwin.strategy.ki.model.Action;
import de.basti.p.sixwin.strategy.ki.model.State;
import de.basti.p.sixwin.viewer.SlotViewer;

public class SlotCardDifFeatureExtractor extends ASlotFeatureExtractor {

	public SlotCardDifFeatureExtractor(int slot) {
		super(slot);
	}

	@Override
	public double extractFeatureValue(State s, Action a) {
		SlotViewer slotViewer = s.getSlots().get(slot);
		return Math.max(0, a.getCard().getNumber() - slotViewer.getHighestNumber());
	}

}
