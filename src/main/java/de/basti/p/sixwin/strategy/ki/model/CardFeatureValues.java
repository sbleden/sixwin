package de.basti.p.sixwin.strategy.ki.model;

import java.util.List;

import de.basti.p.sixwin.Card;

public class CardFeatureValues {

	private List<Double> featuresValues;
	private Card c;

	public CardFeatureValues(List<Double> featuresValues, Card c) {
		super();
		this.featuresValues = featuresValues;
		this.c = c;
	}

	public Card getC() {
		return c;
	}

	public List<Double> getFeaturesValues() {
		return featuresValues;
	}

}
