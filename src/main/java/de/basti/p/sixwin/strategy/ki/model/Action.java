package de.basti.p.sixwin.strategy.ki.model;

import de.basti.p.sixwin.Card;

public class Action {

	private Card card;

	public Action(Card card) {
		super();
		this.card = card;
	}

	public Card getCard() {
		return card;
	}
}
