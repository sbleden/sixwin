package de.basti.p.sixwin.strategy.ki.features;

import java.util.OptionalInt;

import de.basti.p.sixwin.Constants;
import de.basti.p.sixwin.strategy.ki.model.Action;
import de.basti.p.sixwin.strategy.ki.model.State;

public class MinDifExtractor implements IFeatureExtractor {

	@Override
	public double extractFeatureValue(State s, Action a) {
		OptionalInt result = s.getSlots().stream().filter(slot -> slot.getCards().size() < 5)
				.mapToInt(slot -> a.getCard().getNumber() - slot.getHighestNumber()).filter(v -> v > 0).min();

		return result.orElse(0) * 1.0 / Constants.NUMBER_OF_CARDS;
	}

}
