package de.basti.p.sixwin.strategy.ki.features.plain;

import de.basti.p.sixwin.strategy.ki.features.IFeatureExtractor;

public abstract class ASlotFeatureExtractor implements IFeatureExtractor {

	protected int slot;

	public ASlotFeatureExtractor(int slot) {

		this.slot = slot;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ":" + slot;
	}
}