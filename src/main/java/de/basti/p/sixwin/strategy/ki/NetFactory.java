package de.basti.p.sixwin.strategy.ki;

import java.io.File;
import java.io.IOException;

import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;

public class NetFactory {

	private static final double learningRate = 1e-2;
	private static final int seed = 12345;

	public static MultiLayerNetwork loadNetwork(String file) {
		try {
			MultiLayerNetwork net = ModelSerializer.restoreMultiLayerNetwork(new File(file));
			return net;
		} catch (IOException e) {
			throw new RuntimeException("Could not load saved network", e);
		}
	}

	public static MultiLayerNetwork createNetwork(boolean withServer, int numInputs, int numOutputs) {
		final MultiLayerConfiguration conf = getDeepDenseLayerNetworkConfiguration(numInputs, numInputs);
		MultiLayerNetwork net = new MultiLayerNetwork(conf);
		net.init();
		if (withServer) {
			startUiServer(net);
		}
		return net;
	}

	private static void startUiServer(MultiLayerNetwork network) {
		// Initialize the user interface backend
		UIServer uiServer = UIServer.getInstance();

		// Configure where the network information (gradients, score vs. time
		// etc) is to
		// be stored. Here: store in memory.
		StatsStorage statsStorage = new InMemoryStatsStorage(); // Alternative:
																// new
																// FileStatsStorage(File),
																// for saving
																// and loading
																// later

		// Attach the StatsStorage instance to the UI: this allows the contents
		// of the
		// StatsStorage to be visualized
		uiServer.attach(statsStorage);
		network.setListeners(new StatsListener(statsStorage));
	}

	private static MultiLayerConfiguration getDeepDenseLayerNetworkConfiguration(int numInputs, int numOutputs) {
		final int numHiddenNodes = numInputs * numInputs;
		return new NeuralNetConfiguration.Builder().seed(seed)
				.gradientNormalization(GradientNormalization.RenormalizeL2PerLayer).weightInit(WeightInit.XAVIER)
				.regularization(true).updater(new Nesterovs(learningRate, 0.9)).list()
				.layer(0,
						new DenseLayer.Builder().nIn(numInputs).nOut(numHiddenNodes).activation(Activation.RELU)
								.build())
				.layer(1,
						new DenseLayer.Builder().nIn(numHiddenNodes).nOut(numHiddenNodes).activation(Activation.RELU)
								.build())
				.layer(2,
						new DenseLayer.Builder().nIn(numHiddenNodes).nOut(numHiddenNodes).activation(Activation.RELU)
								.build())
				.layer(3,
						new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
								.activation(Activation.SOFTMAX).weightInit(WeightInit.XAVIER).nIn(numHiddenNodes)
								.nOut(numOutputs).build())
				.build();
	}

}
