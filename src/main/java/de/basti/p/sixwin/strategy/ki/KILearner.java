package de.basti.p.sixwin.strategy.ki;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.deeplearning4j.util.ModelSerializer;

import com.google.common.collect.Lists;

import de.basti.p.sixwin.GameManager;
import de.basti.p.sixwin.GameResult;
import de.basti.p.sixwin.IPlayerStrategy;
import de.basti.p.sixwin.strategy.RandomStrategy;
import de.basti.p.sixwin.strategy.SimpleStrategy;
import de.basti.p.sixwin.strategy.ki.features.IFeatureExtractor;
import de.basti.p.sixwin.strategy.ki.model.SituationFeatureValues;
import de.basti.p.sixwin.strategy.ki.v1.CardChoosingNetwork;
import de.basti.p.sixwin.strategy.ki.v1.KIStrategy;

public class KILearner {

	private final int numEpochs = 100_000;
	private final int validationFrequency = 1000;
	private final int gamesPerEpoch = 100;

	private final List<IFeatureExtractor> featureExtractors;
	private final CardChoosingNetwork cardChoosingNetwork;

	public KILearner(List<IFeatureExtractor> featureExtractors, CardChoosingNetwork cardChoosingNetwork) {
		super();
		this.featureExtractors = featureExtractors;
		this.cardChoosingNetwork = cardChoosingNetwork;

	}

	public void doRun() throws IOException {

		KIStrategy kiStrategy = new KIStrategy("Current Network", cardChoosingNetwork, featureExtractors);
		RandomStrategy randomStrategy = new RandomStrategy();
		SimpleStrategy simpleStrategy = new SimpleStrategy();

		ActionRecordingStrategy gameRecordingStrategy1 = new ActionRecordingStrategy(randomStrategy, featureExtractors);
		ActionRecordingStrategy gameRecordingStrategy2 = new ActionRecordingStrategy(randomStrategy, featureExtractors);
		ActionRecordingStrategy gameRecordingStrategy3 = new ActionRecordingStrategy(simpleStrategy, featureExtractors);
		ActionRecordingStrategy gameRecordingStrategy4 = new ActionRecordingStrategy(simpleStrategy, featureExtractors);

		List<IPlayerStrategy> strategies = Lists.newArrayList(gameRecordingStrategy1, gameRecordingStrategy2,
				gameRecordingStrategy3, gameRecordingStrategy4);

		List<IPlayerStrategy> validationStrategy = Lists.newArrayList(kiStrategy, randomStrategy, new SimpleStrategy(),
				new SimpleStrategy());

		for (int e = 0; e < numEpochs; e++) {
			List<SituationFeatureValues> bestActions = Lists.newArrayList();
			runGames(strategies, bestActions, gamesPerEpoch);
			cardChoosingNetwork.fit(bestActions);

			if (e % validationFrequency == 0) {
				GameResult validationResult = runGames(validationStrategy, Lists.newArrayList(), gamesPerEpoch * 5);
				System.out.println(validationResult);
				ModelSerializer.writeModel(cardChoosingNetwork.getNet(), new File("v1.model"), false);
			}
		}

	}

	private GameResult runGames(List<IPlayerStrategy> strategies, List<SituationFeatureValues> bestActions,
			int numGames) {
		GameResult result = null;
		for (int i = 0; i < numGames; i++) {
			GameManager gameManager = new GameManager(strategies, false);
			GameResult gameResult = gameManager.runGame();

			if (result == null) {
				result = gameResult;
			} else {
				result = result.merge(gameResult);
			}

			for (IPlayerStrategy strategy : strategies) {
				if (strategy instanceof ActionRecordingStrategy) {
					ActionRecordingStrategy actionRecordingStrategy = (ActionRecordingStrategy) strategy;
					if (strategy == gameResult.getWinner()) {
						bestActions.addAll(actionRecordingStrategy.getActions());
					}
					actionRecordingStrategy.clearActions();
				}
			}
		}
		return result;
	}

}
