package de.basti.p.sixwin;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.beust.jcommander.internal.Maps;

public class GameResult {

	private final Map<IPlayerStrategy, Double> strategyToPointsMap;
	private final Map<IPlayerStrategy, Integer> strategyToWinsMap;
	private final int count;

	public GameResult(Map<IPlayerStrategy, Double> strategyToPointsMap, Map<IPlayerStrategy, Integer> strategyToWinsMap,
			int count) {
		super();
		this.strategyToPointsMap = strategyToPointsMap;
		this.strategyToWinsMap = strategyToWinsMap;
		this.count = count;
	}

	public GameResult(Map<IPlayerStrategy, Double> strategyToPointsMap, IPlayerStrategy winner) {
		super();
		this.count = 1;
		this.strategyToPointsMap = strategyToPointsMap;
		this.strategyToWinsMap = Maps.newHashMap();
		Set<IPlayerStrategy> keySet = strategyToPointsMap.keySet();
		for (IPlayerStrategy strategy : keySet) {
			strategyToWinsMap.put(strategy, strategy == winner ? 1 : 0);
		}
	}

	public Map<IPlayerStrategy, Double> getStrategyToPointsMap() {
		return strategyToPointsMap;
	}

	public Map<IPlayerStrategy, Integer> getStrategyToWinsMap() {
		return strategyToWinsMap;
	}

	public GameResult merge(GameResult result) {
		if (count == 0) {
			return result;
		}
		Map<IPlayerStrategy, Double> strategyToPointsMap = new HashMap<IPlayerStrategy, Double>();
		Map<IPlayerStrategy, Integer> strategyToWinsMap = new HashMap<IPlayerStrategy, Integer>();
		for (IPlayerStrategy str : this.strategyToPointsMap.keySet()) {
			double currentValue = this.strategyToPointsMap.get(str);
			double newValue = result.getStrategyToPointsMap().get(str);
			double newWeigtetValue = (currentValue * count + newValue * result.count) / (count + result.count);
			strategyToPointsMap.put(str, newWeigtetValue);

			int currentWins = this.strategyToWinsMap.get(str);
			int otherWins = result.getStrategyToWinsMap().get(str);
			int newWins = currentWins + otherWins;
			strategyToWinsMap.put(str, newWins);
		}
		return new GameResult(strategyToPointsMap, strategyToWinsMap, count + result.count);
	}

	public IPlayerStrategy getWinner() {
		return strategyToPointsMap.keySet().stream().min(Comparator.comparing(strategyToPointsMap::get)).get();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		strategyToPointsMap.forEach((s, i) -> {
			builder.append(s.getName());
			builder.append(": Wins:");
			builder.append(strategyToWinsMap.get(s));
			builder.append(". Average Points:");
			builder.append(i);
			builder.append("\n");
		});
		return builder.toString();
	}

}
